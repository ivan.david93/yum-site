
var app = angular.module('myApp', ['pascalprecht.translate', 'ngRoute']);

// to make rout to pages, with this without refresh only the oncnten on ng-view is changed
// inserting content from main.htm,blog.htm inside main div
app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "main.htm"
    })
    .when("/blog", {
      templateUrl: "blog.htm"
      // TODO add blogController here
    })
    .when("/traducoes", {
      templateUrl: "traducoes.htm"
      // TODO add blogController here
    });

});

app.config(function ($translateProvider) {
  $translateProvider.preferredLanguage('pt');
  $translateProvider.useStaticFilesLoader({
    prefix: '../languages/locale-',
    suffix: '.json'
  });
});

app.controller('TranslateController', function ($translate, $scope, $http) {

  $scope.$watch('selectedCountry', function() {
    //alert('changed');
    console.log($scope.selectedCountry);
});

  $scope.countries = [{
    name: "Português",
    img: "img/logos/flags/portugal.png",
    initials: "pt"
  }, {
    name: "English",
    img: "img/logos/flags/united-kingdom.png",
    initials: "en"
  }, {
    name: "Deutsch",
    img: "img/logos/flags/germany.png",
    initials: "de"
  }, {
    name: "Français",
    img: "img/logos/flags/france.png",
    initials: "fr"
  }, {
    name: "中文",
    img: "img/logos/flags/china.png",
    initials: "cn"
  }];

 
  $scope.selectedCountry =  $scope.countries[0];

  $scope.hoverMe = function () {
    //console.log("hoverMe");
    $('#langPick').attr('size', $scope.countries.length+1);
    // setTimeout(function() {
    //     document.getElementById('langX').click();
    //     $scope.clicked = true;
    // }, 3000);
  };

  $scope.leftMe = function () {
    //console.log("leftMe");
    $('#langPick').attr('size', 1);
    // setTimeout(function() {
    //     document.getElementById('langX').click();
    //     $scope.clicked = true;
    // }, 3000);
  };

  $scope.submitForm = function () {
    $http.post('/api/submitForm', $scope.formData)
      .success(function (data) {
        $scope.formData = {};
        $scope.todos = data;
      })
      .error(function (data) {
        console.log('Error: ' + data);
      });
  };

  $scope.changeLanguage = function (langKey) {

    $translate.use($scope.selectedCountry.initials);
  };


  $scope.languageBrowser = window.navigator.language || $window.navigator.userLanguage;
  $translate.use($scope.languageBrowser.substring(0, 2));
  //$scope.credentials.userLocale = window.navigator.language.replace('-','_');
  console.log($scope.languageBrowser);
});

app.controller('LoginController', function ($translate, $scope, $http) {
  console.log("enter LoginController");
  $scope.formData = {};

  $scope.loginBO = function () {
    $http.post('/api/login', $scope.formData)
      .then(function (result) {
        $scope.user = result;
        console.log(result);
      }, function (result) {
        //some error
        console.log(result);
      });
  };

  //register to backoffice
  $scope.registerBO = function () {
    $http.post('/api/register', $scope.formData)
      .then(function (result) {
        $scope.user = result;
        console.log(result);
      }, function (result) {
        //some error
        console.log(result);
      });
  };
});


