(function ($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 54)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function () {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Collapse the navbar when page is scrolled
  $(window).scroll(function () {
    if ($(window).width() > 991) {

      if ($("#mainNav").offset().top > 100) {
        $("#logo1").show();
        $("#logo2").hide();
        $("#mainNav").addClass("navbar-shrink");
        $("#langPick").addClass("navbar-shrink");
      } else {
        $("#logo1").hide();
        $("#logo2").show();
        $("#mainNav").removeClass("navbar-shrink");
        $("#langPick").removeClass("navbar-shrink");
      }
    } else {
      $("#logo2").show();
    }
  });




})(jQuery); // End of use strict
