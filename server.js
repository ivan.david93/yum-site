// server.js

// set up ========================
var express = require('express');
var app = express();                               // create our app w/ express
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

var bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

var session = require('express-session');


// configuration =================

//connect to mLab 
//var url = 'mongodb://admin:admin@ds113455.mlab.com:13455/yum-site-db';
//connect to localhost 
var url = 'mongodb://localhost:27017/yum-site-db';
//mongoose.connect(url);     // connect to mongoDB database on modulus.io

app.use(express.static(__dirname + '/app/public'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({ 'extended': 'true' }));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json


//use sessions for tracking logins
app.use(session({
    secret: 'work hard',
    resave: true,
    saveUninitialized: false
}));

app.use(methodOverride());

// define model =================
// DONT DEPLOY THIS  DATABASE MISSING 
/* var Todo = mongoose.model('Todo', {
    text: String
}); */
/* 
var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    passwordConf: {
        type: String,
        required: true,
    }
});

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
    var user = this;
    bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
            return next(err);
        }
        user.password = hash;
        next();
    })
});

//authenticate input against database
UserSchema.statics.authenticate = function (email, password, callback) {
    User.findOne({ email: email })
        .exec(function (err, user) {
            if (err) {
                return callback(err)
            } else if (!user) {
                var err = new Error('User not found.');
                err.status = 401;
                return callback(err);
            }
            bcrypt.compare(password, user.password, function (err, result) {
                if (result === true) {
                    return callback(null, user);
                } else {
                    return callback();
                }
            })
        });
}

var User = mongoose.model('User', UserSchema);
module.exports = User; */

app.post('/api/submitForm', function (req, res) {
    //nodemailer
    var nodemailer = require('nodemailer');

    let transporter = nodemailer.createTransport({
        service: 'gmail',
        secure: false,
        port: 25,
        auth: {
            user: 'testeyum@gmail.com',
            pass: 'testeyum*#'
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    let HelperOptions = {
        from: '"YUM" <testeyum@gmail.com',
        to: 'testeyum@gmail.com',
        subject: 'New message from YUM',
        text: 'Name: ' + req.body.name + ' \nEmail: ' + req.body.email + '\nPhone: ' + req.body.phone + '\nMessage: ' + req.body.text
    };
    transporter.sendMail(HelperOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log("sended");
        console.log(info);
    });
    //---------------------------------end nodemailer----------------------------------------------


});

// delete a todo
app.delete('/api/todos/:todo_id', function (req, res) {
    Todo.remove({
        _id: req.params.todo_id
    }, function (err, todo) {
        if (err)
            res.send(err);

        // get and return all the todos after you create another
        Todo.find(function (err, todos) {
            if (err)
                res.send(err)
            res.json(todos);
        });
    });
});

// -----------------------------   login form ----------------------

app.post('/api/login', function (req,res) {

     if (req.body.logemail && req.body.logpassword) {
  
           User.authenticate(req.body.logemail, req.body.logpassword, function (error, user) {
               if (error || !user) {
                 var err = new Error('Wrong email or password.');
                 err.status = 401;
                 //return next(err);
             } 
             else {
                 req.session.userId = user._id;
                 return res.redirect('/profile');
               }
             });
           } else {
                var err = new Error('All fields required.');
                err.status = 400;
                return next(err);
           }
     });

app.post('/api/register', function (req, res) {

    if (req.body.email &&
        req.body.username &&
        req.body.password &&
        req.body.password) {
        var userData = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
          passwordConf: req.body.password,
        }
        //use schema.create to insert data into the db
        User.create(userData, function (err, user) {
            if (err) {
                return next(err)
            } else {
                return res.redirect('/profile');
                //res.redirect(301, '/');
            }
        });
    }

});
//---------------------------------end login----------------------------------------------

// application ------------------------------------------------------------
app.get('/login', function (req, res) {
    //res.sendfile('./app/public/login.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.get('/translate', function (req, res) {
    //res.sendfile('./app/public/views/main.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.get('*', function (req, res) {
    res.sendfile('./app/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

// listen (start app with node server.js) ======================================
app.listen(process.env.PORT || 8080, function () {
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
console.log("App listening on port 8080");